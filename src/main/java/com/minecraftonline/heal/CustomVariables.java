package com.minecraftonline.heal;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class CustomVariables {

	// **Below are the default values**.
	// Time the heal cooldown lasts in seconds.
	static int coolDownTimeHeal = 5;
	// Time the feed cooldown lasts in seconds.
	static int coolDownTimeFeed = 30;
	// Number of hearts to deduct (6 represents 3 hearts).
	static int healthDamagePenalty = 6;
	// Will the health penalty kill the player or send them to half a heart?
	static boolean healIsKillable = false;
	// The player's saturation level upon using /feed.
	static double saturationLevel = 5.0;

	static void getConfigValues() {
		// Creates parent directories if they do not already exist.
		new File("config/heal").mkdirs();
		// Path to the configuration file which contains custom values for the variables
		// coolDownTimeHeal, coolDownTimeFeed, healthDamagePenalty, healIsKillable,
		// and saturationLevel.
		String filePath = new File("config/heal").getAbsolutePath() + "/heal.conf";
		try {
			List<String> lines = Files.readAllLines(Paths.get(filePath));
			coolDownTimeHeal = Integer.parseInt(lines.get(1).split("=")[1]);
			coolDownTimeFeed = Integer.parseInt(lines.get(2).split("=")[1]);
			healthDamagePenalty = Integer.parseInt(lines.get(3).split("=")[1]);
			healIsKillable = Boolean.parseBoolean(lines.get(4).split("=")[1]);
			saturationLevel = Double.parseDouble(lines.get(5).split("=")[1]);
		} catch (IOException | NumberFormatException | IndexOutOfBoundsException e) {
			// If there are any errors, the configuration file gets completely reset.
			resetConfigFile(filePath);
		}
	}

	private static void resetConfigFile(String filePath) {
		// First the file gets deleted.
		try {
			Files.delete(Paths.get(filePath));
		} catch (IOException e) {
			// Left intentionally blank.
		}
		try {
			// Creates and appends to the new configuration file.
			Files.write(Paths.get(filePath),
					("#APART FROM THIS ONE, COMMENTS ARE NOT SUPPORTED. DO NOT ADD LINES OR CHANGE THE POSITIONING OF ANYTHING."
					+ "\ncoolDownTimeHeal=5"
					+ "\ncoolDownTimeFeed=30"
					+ "\nhealthDamagePenalty=6"
					+ "\nhealIsKillable=false"
					+ "\nsaturationLevel=5")
							.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			// Left intentionally blank
		}
	}
}
