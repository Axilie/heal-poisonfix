package com.minecraftonline.heal;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.PotionEffectData;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.living.player.Player;

import java.util.List;
import java.util.Optional;

public class ClearPotionEffects {

	static boolean clearSpecificEffect(Player user, String potionEffect) {
		boolean changedState = false;
		Optional<PotionEffectData> playerPotionEffects = user.get(PotionEffectData.class);
		if (playerPotionEffects.isPresent()) {
			List<PotionEffect> effects = user.get(Keys.POTION_EFFECTS).get();
			for (int i = 0; i < effects.size(); i++) {
				if (effects.get(i).toString().split(",")[0].equals(potionEffect)) {
					effects.remove(i);
					user.offer(Keys.POTION_EFFECTS, effects);
					changedState = true;
					break;
				}
			}
		}
		return changedState;
	}
}
